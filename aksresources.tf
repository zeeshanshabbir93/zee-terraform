
provider "helm" {
  kubernetes {
    username               = data.azurerm_kubernetes_cluster.cluster_data.kube_admin_config.0.username
    password               = data.azurerm_kubernetes_cluster.cluster_data.kube_admin_config.0.password
    host                   = data.azurerm_kubernetes_cluster.cluster_data.kube_admin_config.0.host
    client_certificate     = base64decode(data.azurerm_kubernetes_cluster.cluster_data.kube_admin_config.0.client_certificate)
    client_key             = base64decode(data.azurerm_kubernetes_cluster.cluster_data.kube_admin_config.0.client_key)
    cluster_ca_certificate = base64decode(data.azurerm_kubernetes_cluster.cluster_data.kube_admin_config.0.cluster_ca_certificate)
  }
}


resource "helm_release" "nginx_ingress" {
  name             = "ingress-nginx"
  repository       = "https://kubernetes.github.io/ingress-nginx"
  chart            = "ingress-nginx"
  create_namespace = true
  timeout          = 600
  namespace        = "ingress-nginx"

  set {
    name  = "kind"
    value = "DaemonSet"
  }

  set {
    name  = "service.enableHttps"
    value = "true"
  }
  set {
    name  = "service.enableHttp"
    value = "true"
  }
  set {
    name  = "controller.service.loadBalancerIP"
    value = azurerm_public_ip.cmms_aks_pip.ip_address
  }
  # set {
  #   name  = "controller.service.annotations.service\\.beta\\.kubernetes\\.io/azure-dns-label-name"
  #   value = azurerm_public_ip.cmms_aks_pip.fqdn
  # }
  depends_on = [
    azurerm_public_ip.cmms_aks_pip
  ]
}





resource "helm_release" "akv2k8s" {
  name             = "akv2k8s"
  repository       = "https://charts.spvapi.no"
  chart            = "akv2k8s"
  create_namespace = true
  timeout          = 600
  namespace        = "akv2k8s"

  values = [
    "${file("akv2k8s.values.yaml")}"
  ]
}



resource "helm_release" "aad_workload_identity" {
  name             = "workload-identity-webhook"
  repository       = "https://azure.github.io/azure-workload-identity/charts"
  chart            = "workload-identity-webhook"
  create_namespace = true
  timeout          = 600
  namespace        = "azure-workload-identity-system"

  set {
    name  = "azureTenantID"
    value = "68282401-d938-43a1-a7a9-bec81ff1bbcd"
  }
}



# output "host" { value = data.azurerm_kubernetes_cluster.cluster_data.kube_admin_config.0.host }
# output "client_certificate" { value = base64decode(data.azurerm_kubernetes_cluster.cluster_data.kube_admin_config.0.client_certificate) }
# output "client_key" { value = base64decode(data.azurerm_kubernetes_cluster.cluster_data.kube_admin_config.0.client_key) }
# output "cluster_ca_certificate" { value = base64decode(data.azurerm_kubernetes_cluster.cluster_data.kube_admin_config.0.cluster_ca_certificate) }
# output "username" { value = (data.azurerm_kubernetes_cluster.cluster_data.kube_admin_config.0.username) }
# output "password" { value = (data.azurerm_kubernetes_cluster.cluster_data.kube_admin_config.0.password) }
