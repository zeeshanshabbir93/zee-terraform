environment_name       = "CMMS Development"
client_secret          = "yxT7Q~oi~Uw1UJjRqrPHapP3oqABj62zhrhqP"
infrastructure_rg_name = "zee-tf-destroy"
region                 = "southeastasia"
vnet_name              = "zee-terraform-vnet-destroy"

address_space       = "10.0.0.0/16"
subnet_prefix       = ["10.0.0.0/20", "10.0.17.0/24", "10.0.18.0/24", "10.0.254.0/24"]
subnet_names        = ["AKSNodesSubnet", "RedisSubnet", "PostgreSQLSubnet", "GatewaySubnet"]
postgres_vnet_rules = ["AKSNodesSubnet", "GatewaySubnet"]

subnet_service_endpoints = {
  "AKSNodesSubnet" : ["Microsoft.KeyVault", "Microsoft.Storage", "Microsoft.Sql"],
  "RedisSubnet" : ["Microsoft.KeyVault", "Microsoft.Storage"],
  "PostgreSQLSubnet" : ["Microsoft.KeyVault", "Microsoft.Sql"],
  "GatewaySubnet" : ["Microsoft.Sql", "Microsoft.KeyVault", "Microsoft.Storage"]
}

subnet_enforce_private_link_endpoint_network_policies = {
  "AKSNodesSubnet" : true,
  "RedisSubnet" : true,
  "PostgreSQLSubnet" : false,
  "GatewaySubnet" : false
}

storage_mb             = "102400"
administrator_login    = "zeetfadmin"
administrator_password = "@Intech#123"
db_names               = ["keycloak", "cmms_tenant", "cmms_default", "grafana", "pds", "superset"]
db_charset             = "UTF8"
db_collation           = "English_United States.1252"

postgresql_configurations = {
  backslash_quote           = "on",
  wal_buffers               = "65536",
  effective_cache_size      = "6291456",
  work_mem                  = "4096",
  maintenance_work_mem      = "262144",
  max_prepared_transactions = "1200",
  shared_preload_libraries = "timescaledb"
}

firewall_rules = [{ "start_ip" : "39.42.52.109", "end_ip" : "39.42.52.109" }]





redis_cache_name                            = "zee-tf-redis"
redis_capacity                              = 1
redis_family                                = "C"
redis_sku                                   = "Standard"
redis_maxmemory_reserved                    = 100
redis_maxfragmentationmemory_reserved       = 100
redis_maxmemory_delta                       = 100
redis_cache_private_endpoint_name           = "cmms-redis-pe-dev-eus"
redis_cache_private_service_connection_name = "cmms-redis-psc-dev-eus"


kubernetes_cluster_name                      = "cmms-aks-dev-eus"
aks_private_cluster_enabled                  = true
aks_private_cluster_public_fqdn_enabled      = false
aks_public_network_access_enabled            = false
aks_automatic_channel_upgrade                = "stable"
aks_dns_service_ip                           = "10.0.16.126"
aks_docker_bridge_cidr                       = "10.0.16.128/25"
aks_service_cidr                             = "10.0.16.0/25"
aks_outbound_type                            = "loadBalancer"
aks_load_balancer_sku                        = "Standard"
aks_default_node_pool_node_count             = 1
aks_default_node_pool_min_count              = 1
aks_default_node_pool_max_count              = 7
aks_default_node_pool_vm_size                = "Standard_D2_v2"
aks_default_node_pool_enable_auto_scaling    = true
aks_default_node_pool_type                   = "VirtualMachineScaleSets"
aks_default_node_pool_max_pods               = 150
aks_default_node_pool_os_disk_size_gb        = 50
aks_default_node_pool_enable_host_encryption = false
log_analytics_workspace_name                 = "cmms-law-dev-eus"

cmms_aks_pip = "cmms_aks_pip"


jfrog_repo_name = "intechww-jfrog-io"



jfrog_docker_config_json = "ewoJImF1dGhzIjogewoJCSJpbnRlY2h3dy1kb2NrZXItbG9jYWwuamZyb2cuaW8iOiB7CgkJCSJhdXRoIjogImRHVmhiWEJzWVhrNlFWQXpUV1V6TlZoSFZITkJaWE0wTjJaQlJVWnRja1ozVEZWdGVUSjJjMjVXY1VWeGNtYz0iCgkJfQoJfQp9"