terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "2.99.0"
    }
    azuread = {
      source  = "hashicorp/azuread"
      version = "2.19.1"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.8.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "2.4.1"
    }
  }
  backend "azurerm" {
    resource_group_name  = "zee-tf-state-rg"
    storage_account_name = "zeetfstateaccount"
    container_name       = "tfstate"
    key                  = "zee.terraform.tfstate"
    use_microsoft_graph  = "true"
  }



}
provider "azuread" {
  client_id     = "685f4b0b-ffb7-479f-b4c4-113683c758dd" # zee-terraform Service Principle
  client_secret = var.client_secret
  tenant_id     = "68282401-d938-43a1-a7a9-bec81ff1bbcd"
}
provider "azurerm" {
  features {
    key_vault {
      purge_soft_delete_on_destroy = true
    }
  }
  tenant_id       = "68282401-d938-43a1-a7a9-bec81ff1bbcd"
  subscription_id = "91092f81-4778-403c-b20f-c345b97eefa8"
  client_id       = "685f4b0b-ffb7-479f-b4c4-113683c758dd" # zee-terraform Service Principle
  client_secret   = var.client_secret

}
# resource "azurerm_resource_group" "terraform_state_rg" {
#   name     = "zee-tf-state-rg"
#   location = "southeastasia"

# }

# resource "azurerm_storage_account" "terraform_state_sa" {
#   name                     = "zeetfstateaccount"
#   resource_group_name      = azurerm_resource_group.terraform_state_rg.name
#   location                 = azurerm_resource_group.terraform_state_rg.location
#   account_tier             = "Standard"
#   account_replication_type = "LRS"
#   allow_blob_public_access = false
#   min_tls_version          = "TLS1_2"

# }

resource "azurerm_resource_group" "infra" {
  name     = var.infrastructure_rg_name
  location = var.region
}

# resource "azuread_group" "tf_ADgroup" {
#   display_name = "Zee_TF_ADgroup"
#   owners = [
#     "e26448a5-5647-47de-aa28-7e9a11c53d12",
#   ]
#   security_enabled = true

#   members = [
#     "e26448a5-5647-47de-aa28-7e9a11c53d12",

#   ]
# }

resource "azurerm_virtual_network" "zee_terraform" {
  name                = var.vnet_name
  location            = azurerm_resource_group.infra.location
  resource_group_name = azurerm_resource_group.infra.name
  address_space       = length(var.address_spaces) == 0 ? [var.address_space] : var.address_spaces
  dns_servers         = var.dns_servers
}

resource "azurerm_subnet" "zee_terraform" {
  count                                          = length(var.subnet_names)
  name                                           = var.subnet_names[count.index]
  resource_group_name                            = azurerm_resource_group.infra.name
  virtual_network_name                           = azurerm_virtual_network.zee_terraform.name
  address_prefixes                               = [var.subnet_prefix[count.index]]
  service_endpoints                              = lookup(var.subnet_service_endpoints, var.subnet_names[count.index], [])
  enforce_private_link_endpoint_network_policies = lookup(var.subnet_enforce_private_link_endpoint_network_policies, var.subnet_names[count.index], false)



}


resource "azurerm_postgresql_server" "zee-tf-pg" {
  name                = "zee-tf-pg"
  location            = azurerm_resource_group.infra.location
  resource_group_name = azurerm_resource_group.infra.name

  administrator_login          = var.administrator_login
  administrator_login_password = var.administrator_password

  sku_name   = "GP_Gen5_2"
  version    = "11"
  storage_mb = var.storage_mb

  backup_retention_days        = 7
  geo_redundant_backup_enabled = false
  auto_grow_enabled            = true

  public_network_access_enabled    = true # should be enabled for vnet service endpoints to work. 
  ssl_enforcement_enabled          = false
  ssl_minimal_tls_version_enforced = "TLSEnforcementDisabled"
}

resource "azurerm_postgresql_database" "dbs" {
  count               = length(var.db_names)
  name                = var.db_names[count.index]
  resource_group_name = azurerm_resource_group.infra.name
  server_name         = azurerm_postgresql_server.zee-tf-pg.name
  charset             = var.db_charset
  collation           = var.db_collation
}

resource "azurerm_postgresql_configuration" "zee-pg-confg" {
  count               = length(keys(var.postgresql_configurations))
  resource_group_name = azurerm_resource_group.infra.name
  server_name         = azurerm_postgresql_server.zee-tf-pg.name
  name                = element(keys(var.postgresql_configurations), count.index)
  value               = element(values(var.postgresql_configurations), count.index)
  depends_on = [
    azurerm_postgresql_server.zee-tf-pg
  ]
}

locals {
  postgres_subnet_ids = [for i in azurerm_subnet.zee_terraform : i.id if contains(var.postgres_vnet_rules, i.name)]
}


resource "azurerm_postgresql_virtual_network_rule" "vnet_rules" {
  count               = length(local.postgres_subnet_ids)
  name                = format("%s%s", var.vnet_rule_name_prefix, count.index)
  resource_group_name = azurerm_resource_group.infra.name
  server_name         = azurerm_postgresql_server.zee-tf-pg.name
  subnet_id           = local.postgres_subnet_ids[count.index]
  depends_on = [
    azurerm_postgresql_server.zee-tf-pg
  ]
}
resource "azurerm_postgresql_firewall_rule" "tf_pg_fw" {
  count               = length(var.firewall_rules)
  name                = format("%s%s", "zeetfpgfw", count.index)
  resource_group_name = azurerm_resource_group.infra.name
  server_name         = azurerm_postgresql_server.zee-tf-pg.name
  start_ip_address    = var.firewall_rules[count.index]["start_ip"]
  end_ip_address      = var.firewall_rules[count.index]["start_ip"]
}





resource "azurerm_subnet" "zeeredisSASubnet" {
  count                = var.redis_family == "P" ? 1 : 0
  name                 = "redisSASubnet"
  resource_group_name  = azurerm_resource_group.infra.name
  virtual_network_name = azurerm_virtual_network.zee_terraform.name
  address_prefixes     = ["10.0.17.0/24"]
  service_endpoints    = ["Microsoft.Storage"]
}
resource "azurerm_storage_account" "redis_cache_sa" {
  count               = var.redis_family == "P" ? 1 : 0
  name                = "rediscachesa"
  resource_group_name = azurerm_resource_group.infra.name

  location                 = azurerm_resource_group.infra.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
  account_kind             = "BlobStorage"

  network_rules {
    default_action             = "Deny"
    virtual_network_subnet_ids = [azurerm_subnet.zeeredisSASubnet[0].id]
  }
}


resource "azurerm_redis_cache" "zee_tf_redis" {
  name                          = var.redis_cache_name
  location                      = azurerm_resource_group.infra.location
  resource_group_name           = azurerm_resource_group.infra.name
  capacity                      = var.redis_capacity
  family                        = var.redis_family
  sku_name                      = var.redis_sku
  enable_non_ssl_port           = true
  minimum_tls_version           = "1.2"
  redis_version                 = 6
  public_network_access_enabled = var.redis_family == "P" ? true : false
  subnet_id                     = var.redis_family == "P" ? azurerm_subnet.zee_terraform[1].id : null

  redis_configuration {
    enable_authentication           = true
    rdb_backup_enabled              = var.redis_family == "P" ? true : false
    rdb_backup_frequency            = var.redis_family == "P" ? 60 : null
    rdb_storage_connection_string   = var.redis_family == "P" ? azurerm_storage_account.redis_cache_sa[0].primary_blob_connection_string : null
    maxmemory_reserved              = var.redis_maxmemory_reserved
    maxfragmentationmemory_reserved = var.redis_maxfragmentationmemory_reserved
    maxmemory_delta                 = var.redis_maxmemory_delta
  }
  lifecycle {
    ignore_changes = [redis_configuration.0.rdb_storage_connection_string]
  }
}

resource "azurerm_private_dns_zone" "private_redis_zone" {
  count               = var.redis_family == "C" ? 1 : 0
  name                = "privatelink.redis.cache.windows.net"
  resource_group_name = azurerm_resource_group.infra.name
}


resource "azurerm_private_dns_zone_virtual_network_link" "redis-pvt-dnszone-vnet-link" {
  count                 = var.redis_family == "C" ? 1 : 0
  name                  = "redis-pvt-dnszone-vnet-link"
  resource_group_name   = azurerm_resource_group.infra.name
  private_dns_zone_name = azurerm_private_dns_zone.private_redis_zone[0].name
  virtual_network_id    = azurerm_virtual_network.zee_terraform.id
  registration_enabled  = false

}

resource "azurerm_private_endpoint" "redis_pe" {
  count               = var.redis_family == "C" ? 1 : 0
  name                = "zee-tf-redis"
  location            = azurerm_resource_group.infra.location
  resource_group_name = azurerm_resource_group.infra.name
  subnet_id           = azurerm_subnet.zee_terraform[1].id

  private_service_connection {
    name                           = "tf-redis-privateserviceconnection"
    private_connection_resource_id = azurerm_redis_cache.zee_tf_redis.id
    subresource_names              = ["redisCache"]
    is_manual_connection           = false
  }
  private_dns_zone_group {
    name = azurerm_private_dns_zone.private_redis_zone[0].name
    private_dns_zone_ids = [
      azurerm_private_dns_zone.private_redis_zone[0].id
    ]
  }
  depends_on = [
    azurerm_private_dns_zone.private_redis_zone[0],
    azurerm_redis_cache.zee_tf_redis
  ]
}