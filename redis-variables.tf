variable "redis_cache_name" {
  type = string
}


variable "redis_capacity" {
  type    = number
  default = 1
}

variable "redis_family" {
  type    = string
  default = "P"
}

variable "redis_sku" {
  type    = string
  default = "Premium"
}

variable "redis_maxmemory_reserved" {
  type    = number
  default = 50
}

variable "redis_maxfragmentationmemory_reserved" {
  type    = number
  default = 50
}

variable "redis_maxmemory_delta" {
  type    = number
  default = 50
}

variable "redis_cache_private_endpoint_name" {
  type    = string
  default = "example-endpoint"
}

variable "redis_cache_private_service_connection_name" {
  type    = string
  default = "example-privateserviceconnection"
}