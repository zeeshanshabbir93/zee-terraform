data "azuread_client_config" "current" {}
data "azurerm_client_config" "current" {}
data "azuread_group" "aks_admin_group" {
  display_name     = "aks_admin_group"
  security_enabled = true
}


output "adg" {
  value       = data.azuread_group.aks_admin_group
  description = "adg."
}

data "azurerm_kubernetes_cluster" "cluster_data" {
  name                = var.kubernetes_cluster_name
  resource_group_name = azurerm_resource_group.infra.name
  depends_on = [
    azurerm_kubernetes_cluster.aks-cmms
  ]
}

data "azurerm_resource_group" "cmms_aks_node_resource_group" {
  name = azurerm_kubernetes_cluster.aks-cmms.node_resource_group

  depends_on = [azurerm_kubernetes_cluster.aks-cmms]
}