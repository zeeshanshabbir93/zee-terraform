variable "client_secret" {
  type = string
}

variable "environment_name" {
  type = string
}
variable "region" {
  type = string
}
variable "infrastructure_rg_name" {
  type = string
}
variable "vnet_name" {
  type = string
}

variable "address_space" {
  type    = string
  default = "10.0.0.0/16"
}
variable "address_spaces" {
  type    = list(string)
  default = []
}

variable "dns_servers" {
  type    = list(string)
  default = []
}

variable "subnet_prefix" {
  type    = list(string)
  default = []
}
variable "subnet_names" {
  type    = list(string)
  default = []
}

variable "subnet_service_endpoints" {
  type = map(list(string))
}

variable "subnet_enforce_private_link_endpoint_network_policies" {
  description = "A map with key (string) `subnet name`, value (bool) `true` or `false` to indicate enable or disable network policies for the private link endpoint on the subnet. Default value is false."
  type        = map(bool)
  default     = {}
}


variable "kubernetes_cluster_name" {
  type = string
}

variable "aks_private_cluster_enabled" {
  type = bool
}

variable "aks_private_cluster_public_fqdn_enabled" {
  type = bool
}

variable "aks_public_network_access_enabled" {
  type = bool
}

variable "aks_automatic_channel_upgrade" {
  type = string
}

variable "aks_dns_service_ip" {
  type = string
}

variable "aks_docker_bridge_cidr" {
  type = string
}

variable "aks_service_cidr" {
  type = string
}

variable "aks_outbound_type" {
  type = string
}

variable "aks_load_balancer_sku" {
  type = string
}

variable "aks_default_node_pool_node_count" {
  type = number
}

variable "aks_default_node_pool_min_count" {
  type = number
}

variable "aks_default_node_pool_max_count" {
  type = number
}

variable "aks_default_node_pool_vm_size" {
  type = string
}

variable "aks_default_node_pool_enable_auto_scaling" {
  type = bool
}

variable "aks_default_node_pool_type" {
  type = string
}

variable "aks_default_node_pool_max_pods" {
  type = number
}

variable "aks_default_node_pool_os_disk_size_gb" {
  type = number
}

variable "aks_default_node_pool_enable_host_encryption" {
  type = bool
}

variable "log_analytics_workspace_name" {
  type = string
}


variable "cmms_aks_pip" {
  type = string
}






variable "jfrog_docker_config_json" {
  type = string
}

variable "jfrog_repo_name" {
  type = string
}