# resource "azurerm_user_assigned_identity" "cmms_aks_identity" {
#   name                = "${var.kubernetes_cluster_name}-assigned-identity"
#   location            = azurerm_resource_group.infra.location
#   resource_group_name = azurerm_resource_group.infra.name
# }

# resource "azurerm_role_assignment" "cmms_aks_dns_assignment" {
#   scope                = azurerm_private_dns_zone.privatelink_aks_dns_zone.id
#   role_definition_name = "Private DNS Zone Contributor"
#   principal_id         = azurerm_user_assigned_identity.cmms_aks_identity.principal_id
# }

# resource "azurerm_role_assignment" "cmms_aks_network_assignment" {
#   scope                = azurerm_virtual_network.zee_terraform.id
#   role_definition_name = "Network Contributor"
#   principal_id         = azurerm_user_assigned_identity.cmms_aks_identity.principal_id
# }

# resource "azuread_group" "aks_admin_group" {
#   display_name = "Zee_TF_ADgroup"
#   owners = ["e26448a5-5647-47de-aa28-7e9a11c53d12"

#   ]
#   security_enabled = true

#   members = [
#     "e26448a5-5647-47de-aa28-7e9a11c53d12"

#   ]
# }

resource "azurerm_kubernetes_cluster" "aks-cmms" {
  name                          = var.kubernetes_cluster_name
  location                      = azurerm_resource_group.infra.location
  resource_group_name           = azurerm_resource_group.infra.name
  dns_prefix                    = "${var.kubernetes_cluster_name}-private-k8s"
  private_cluster_enabled       = false
  automatic_channel_upgrade     = var.aks_automatic_channel_upgrade
  public_network_access_enabled = true
  sku_tier                      = "Free"

  default_node_pool {
    name                   = "agentpool"
    node_count             = var.aks_default_node_pool_node_count
    min_count              = var.aks_default_node_pool_min_count
    max_count              = var.aks_default_node_pool_max_count
    vm_size                = var.aks_default_node_pool_vm_size
    enable_auto_scaling    = var.aks_default_node_pool_enable_auto_scaling
    type                   = var.aks_default_node_pool_type
    max_pods               = var.aks_default_node_pool_max_pods
    os_disk_size_gb        = var.aks_default_node_pool_os_disk_size_gb
    vnet_subnet_id         = azurerm_subnet.zee_terraform[0].id
    enable_host_encryption = var.aks_default_node_pool_enable_host_encryption

    enable_node_public_ip = false
  }
  network_profile {
    network_plugin     = "azure"
    network_mode       = "transparent"
    dns_service_ip     = var.aks_dns_service_ip
    docker_bridge_cidr = var.aks_docker_bridge_cidr
    service_cidr       = var.aks_service_cidr
    outbound_type      = var.aks_outbound_type
    load_balancer_sku  = var.aks_load_balancer_sku

    load_balancer_profile {
      outbound_ports_allocated  = 25000
      managed_outbound_ip_count = 5
    }

    # nat_gateway_profile {
    #   managed_outbound_ip_count = 5
    # }
  }
  azure_active_directory_role_based_access_control {
    managed                = true
    tenant_id              = data.azuread_client_config.current.tenant_id
    admin_group_object_ids = [data.azuread_group.aks_admin_group.object_id]
    azure_rbac_enabled     = true

  }

  identity {
    type = "SystemAssigned"
    # user_assigned_identity_id = azurerm_user_assigned_identity.cmms_aks_identity.id
  }

  tags = {
    Environment = "Production"
  }
  # depends_on = [
  #   azurerm_role_assignment.cmms_aks_network_assignment
  # ]

}


resource "azurerm_public_ip" "cmms_aks_pip" {

  name                = var.cmms_aks_pip
  location            = data.azurerm_resource_group.cmms_aks_node_resource_group.location
  resource_group_name = azurerm_kubernetes_cluster.aks-cmms.node_resource_group
  sku                 = "Standard"
  allocation_method   = "Static"
  domain_name_label   = "zee-cmms-nginx"

  depends_on = [
    azurerm_kubernetes_cluster.aks-cmms
  ]
}