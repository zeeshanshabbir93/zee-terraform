resource "azuread_application" "cmms_demo_ad_app" {
  display_name = "zeedemoapp"
  required_resource_access {
    resource_app_id = "00000003-0000-0000-c000-000000000000"

    resource_access {
        id   = "e1fe6dd8-ba31-4d61-89e7-88639da4683d"
        type = "Scope"
    }
  }
}

resource "azuread_service_principal" "cmms_demo_ad_app_sp" {
  application_id               = azuread_application.cmms_demo_ad_app.application_id
  app_role_assignment_required = false
  owners                       = [data.azuread_client_config.current.object_id]
}

resource "azurerm_role_assignment" "cmms_demo_keyvault_administrator" {
  scope                = azurerm_key_vault.zee_tf_kv.id
  role_definition_name = "Key Vault Administrator"
  principal_id         = azuread_service_principal.cmms_demo_ad_app_sp.object_id

  depends_on = [
    azurerm_key_vault.zee_tf_kv
  ]
}


resource "azuread_application_federated_identity_credential" "cmms_demo_ad_federation" {
  application_object_id = azuread_application.cmms_demo_ad_app.object_id
  display_name          = "demoazure-workload-zees"
  description           = "Federated Service Account for demoazure/cmms-demo-demoazure"
  audiences             = ["api://AzureADTokenExchange"]
  issuer                = "https://oidc.prod-aks.azure.com/58543828-08c9-4df1-a6b0-277f2cae88ff/"
  subject               = "system:serviceaccount:demoazure:r1-demoazure"
}