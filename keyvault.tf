resource "azurerm_key_vault" "zee_tf_kv" {
  name                        = "zee-tf-kv"
  location                    = azurerm_resource_group.infra.location
  resource_group_name         = azurerm_resource_group.infra.name
  enabled_for_disk_encryption = true
  tenant_id                   = data.azurerm_client_config.current.tenant_id
  soft_delete_retention_days  = 7
  purge_protection_enabled    = false
  enable_rbac_authorization   = true
  sku_name                    = "standard"
  network_acls {
    default_action             = "Deny"
    ip_rules                   = ["72.255.57.122"]
    bypass                     = "AzureServices"
    virtual_network_subnet_ids = [azurerm_subnet.zee_terraform[0].id]
  }

}


resource "azurerm_role_assignment" "rbac_keyvault_administrator" {
  scope                = azurerm_key_vault.zee_tf_kv.id
  role_definition_name = "Key Vault Administrator"
  principal_id         = data.azurerm_client_config.current.object_id

  depends_on = [
    azurerm_key_vault.zee_tf_kv
  ]
}

# resource "azurerm_role_assignment" "rbac_keyvault_secrets_officer" {
#   for_each             = toset(local.key_vault_users)
#   scope                = azurerm_key_vault.cmms_kv.id
#   role_definition_name = "Key Vault Secrets Officer"
#   principal_id         = each.key

#   depends_on = [
#     azurerm_key_vault.cmms_kv
#   ]
# }

resource "azurerm_role_assignment" "rbac_keyvault_secrets_reader" {
  scope                = azurerm_key_vault.zee_tf_kv.id
  role_definition_name = "Key Vault Secrets Officer"
  principal_id         = data.azurerm_kubernetes_cluster.cluster_data.kubelet_identity[0].object_id

  depends_on = [
    azurerm_key_vault.zee_tf_kv
  ]
}




resource "azurerm_key_vault_secret" "redis_cache_primary_access_key" {
  name         = join("", [var.redis_cache_name, "-primary-access-key"])
  value        = azurerm_redis_cache.zee_tf_redis.primary_access_key
  key_vault_id = azurerm_key_vault.zee_tf_kv.id

  tags = {
    "Created By"  = "Terraform",
    "Environment" = var.environment_name,
    "Purpose"     = join("-", [var.environment_name, var.redis_cache_name, "primary-access-key"]),
    "Region"      = var.region,
    "Resource"    = "Key Vault Secret"
  }

  depends_on = [
    azurerm_key_vault.zee_tf_kv,
    azurerm_redis_cache.zee_tf_redis,
    azurerm_role_assignment.rbac_keyvault_administrator
  ]
}

resource "azurerm_key_vault_secret" "postgresql_administrator_password" {
  name         = join("", ["zee-tf-pg", "-administrator-password"])
  value        = var.administrator_password
  key_vault_id = azurerm_key_vault.zee_tf_kv.id

  tags = {
    "Created By"  = "Terraform",
    "Environment" = var.environment_name,
    "Purpose"     = join("-", [var.environment_name, "zee-tf-pg", "administrator-password"]),
    "Region"      = var.region,
    "Resource"    = "PostgreSQL Administrator Password"
  }

  depends_on = [
    azurerm_key_vault.zee_tf_kv,
    azurerm_postgresql_server.zee-tf-pg,
    azurerm_role_assignment.rbac_keyvault_administrator
  ]
}

resource "azurerm_key_vault_secret" "jfrog_image_pull_secret" {
  name         = join("", [var.jfrog_repo_name, "-docker-pull-secret"])
  value        = var.jfrog_docker_config_json
  key_vault_id = azurerm_key_vault.zee_tf_kv.id

  tags = {
    "Created By"  = "Terraform",
    "Environment" = var.environment_name,
    "Purpose"     = join("-", [var.environment_name, var.jfrog_repo_name, "docker-pull-secret"]),
    "Region"      = var.region,
    "Resource"    = "Key Vault Secret"
  }

  depends_on = [
    azurerm_key_vault.zee_tf_kv,
    azurerm_role_assignment.rbac_keyvault_administrator
  ]
}